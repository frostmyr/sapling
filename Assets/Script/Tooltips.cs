﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Tooltips : MonoBehaviour
{
    static float FADE_RATE = 0.15f;

    static Tooltips Active;

    public TextMeshProUGUI text;
    public RectTransform textRect;
    public RectTransform borderLeft;
    public RectTransform borderRight;
    public RectTransform borderTop;
    public RectTransform borderBottom;

    Vector2 boundary;
    RectTransform rect;
    CanvasGroup canvas;
    Coroutine fadeCoroutine;
    RectTransform target;
    TooltipsPosition position;

    public enum TooltipsPosition { Right, Left, Top, Bottom }
    
    void Start()
    {
        Active = this;
        rect = GetComponent<RectTransform>();
        canvas = GetComponent<CanvasGroup>();
        gameObject.SetActive(false);
    }

    void Update()
    {
        if (target != null)
        {
            if (position == TooltipsPosition.Right)
                rect.position = target.position + (target.sizeDelta.x + rect.sizeDelta.x / 2) * Vector3.right;
            else if (position == TooltipsPosition.Left)
                rect.position = target.position + (target.sizeDelta.x + rect.sizeDelta.x / 2) * Vector3.left;
            else if (position == TooltipsPosition.Top)
                rect.position = target.position + (target.sizeDelta.y + rect.sizeDelta.y / 2) * Vector3.up;
            else if (position == TooltipsPosition.Bottom)
                rect.position = target.position + (target.sizeDelta.y + rect.sizeDelta.y / 2) * Vector3.down;
            rect.anchoredPosition = new Vector2(Mathf.Max(Mathf.Min(rect.anchoredPosition.x, boundary.x), -boundary.x), Mathf.Max(Mathf.Min(rect.anchoredPosition.y, boundary.y), -boundary.y));
        }
    }

    void OnApplicationQuit()
    {
        if (Active == this)
            Active = null;
    }

    public static void Show(string str, RectTransform target = null, TooltipsPosition pos = TooltipsPosition.Right)
    {
        if (Active)
        {
            Active.enabled = true;
            Active.text.text = str;
            Active.textRect.sizeDelta = new Vector2(Mathf.Min(Active.text.preferredWidth, 325), Active.text.preferredHeight);
            Active.rect.sizeDelta = new Vector2(Mathf.Min(Active.text.preferredWidth + 10, 325), Active.text.preferredHeight + 10);
            Active.borderLeft.sizeDelta = Active.borderRight.sizeDelta = new Vector2(1, Active.rect.sizeDelta.y);
            Active.borderTop.sizeDelta = Active.borderBottom.sizeDelta = new Vector2(Active.rect.sizeDelta.x, 1);
            Active.gameObject.SetActive(true);
            Active.boundary = new Vector2((1366 - Active.rect.sizeDelta.x) / 2, (768 - Active.rect.sizeDelta.y) / 2);
            if (target == null)
                Active.rect.anchoredPosition = Vector2.zero;
            else
            {
                Active.position = pos;
                Active.target = target;
                if (pos == TooltipsPosition.Right)
                    Active.rect.position = target.position + (target.sizeDelta.x + Active.rect.sizeDelta.x / 2) * Vector3.right;
                else if (pos == TooltipsPosition.Left)
                    Active.rect.position = target.position + (target.sizeDelta.x + Active.rect.sizeDelta.x / 2) * Vector3.left;
                else if (pos == TooltipsPosition.Top)
                    Active.rect.position = target.position + (target.sizeDelta.y + Active.rect.sizeDelta.y / 2) * Vector3.up;
                else if (pos == TooltipsPosition.Bottom)
                    Active.rect.position = target.position + (target.sizeDelta.y + Active.rect.sizeDelta.y / 2) * Vector3.down;
                Active.rect.anchoredPosition = new Vector2(Mathf.Max(Mathf.Min(Active.rect.anchoredPosition.x, Active.boundary.x), -Active.boundary.x), Mathf.Max(Mathf.Min(Active.rect.anchoredPosition.y, Active.boundary.y), -Active.boundary.y));
            }
            if (Active.fadeCoroutine != null)
                PeriodicTask.StopTask(Active.fadeCoroutine);
            Active.fadeCoroutine = PeriodicTask.StartPeriodicTask(0.03f, () =>
            {
                Active.canvas.alpha += FADE_RATE;
                if (Active.canvas.alpha >= 1)
                {
                    PeriodicTask.StopCurrentTask();
                }
            });
        }
    }

    public static void Hide()
    {
        if (Active)
        {
            if (Active.fadeCoroutine != null)
                PeriodicTask.StopTask(Active.fadeCoroutine);
            Active.fadeCoroutine = PeriodicTask.StartPeriodicTask(0.03f, () =>
            {
                Active.canvas.alpha -= FADE_RATE;
                if (Active.canvas.alpha <= 0)
                {
                    Active.enabled = false;
                    Active.target = null;
                    Active.text.text = "";
                    Active.gameObject.SetActive(false);
                    PeriodicTask.StopCurrentTask();
                }
            });
        }
    }
}
