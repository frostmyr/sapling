﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PanelDrag : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    static PanelDrag ActiveInstance = null;
    static Vector2 DragStartPos;

    public CPanel panel;

    RectTransform Parent;
    Vector2 Boundary;

    // Use this for initialization
    void Start ()
    {
        Parent = transform.parent.parent.GetComponent<RectTransform>();
        Boundary = new Vector2((1366 - Parent.sizeDelta.x) / 2, (768 - Parent.sizeDelta.y) / 2);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (ActiveInstance == this)
        {
            Parent.anchoredPosition = new Vector2(Mathf.Min(Mathf.Max(Input.mousePosition.x - DragStartPos.x, -Boundary.x), Boundary.x), Mathf.Min(Mathf.Max(Input.mousePosition.y - DragStartPos.y, -Boundary.y), Boundary.y));
            DragStartPos = new Vector2(Input.mousePosition.x - Parent.anchoredPosition.x, Input.mousePosition.y - Parent.anchoredPosition.y);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!ActiveInstance)
        {
            ActiveInstance = this;
            DragStartPos = new Vector2(eventData.position.x - Parent.anchoredPosition.x, eventData.position.y - Parent.anchoredPosition.y);
            Workspace.Active.SetActivePanel(panel);
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (ActiveInstance)
        {
            ActiveInstance = null;
        }
    }
}
