﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Workspace : MonoBehaviour
{
    public static Workspace Active;

    static Vector2 BOTTOM_RIGHT = new Vector2(1, -1);
    static Vector2 DEFAULT_POS = new Vector2(-26, 45);

    Vector2 Boundary;

    public CPanel[] Panels;
    public CPanel ActivePanel;
    public List<CPanel> ActivePanelStack = new List<CPanel>();

	public void SetActivePanel(CPanel panel)
    {
        if (panel != ActivePanel)
        {
            if (ActivePanelStack.Contains(panel))
                ActivePanelStack.Remove(panel);
            else if (ActivePanel)
            {
                Vector2 pos = new Vector2(ActivePanel.rect.anchoredPosition.x + 15, ActivePanel.rect.anchoredPosition.y - 15);
                if (pos.x > (1366 - panel.rect.sizeDelta.x) / 2 || pos.y < -(768 - panel.rect.sizeDelta.y) / 2)
                    pos = DEFAULT_POS;
                panel.rect.anchoredPosition = pos;
            }
            ActivePanelStack.Add(panel);
            ActivePanel = panel;
            foreach (CPanel p in Panels)
            {
                p.active = p == panel;
            }
            panel.gameObject.SetActive(true);
        }
    }

    public void DeactivatePanel(CPanel panel)
    {
        ActivePanelStack.Remove(panel);
        panel.gameObject.SetActive(false);
        if (ActivePanelStack.Count > 0)
            SetActivePanel(ActivePanelStack[ActivePanelStack.Count - 1]);
        else
            ActivePanel = null;
    }

    void Start()
    {
        Active = this;
        foreach (CPanel p in Panels)
        {
            p.workspace = this;
        }
    }
}
