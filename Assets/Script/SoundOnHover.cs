﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.EventSystems;

public class SoundOnHover : MonoBehaviour, IPointerEnterHandler
{
    static SoundOnHover Last;
    static Stopwatch Delay = new Stopwatch();

    public AudioSource src;

    public void Start()
    {
        Delay.Start();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (Last != this && Delay.Elapsed.TotalSeconds > 0.1f)
        {
            Last = this;
            src.Play();
            Delay.Reset();
            Delay.Start();
        }
    }
}
