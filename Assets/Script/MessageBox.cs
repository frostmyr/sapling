﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class MessageBox : MonoBehaviour
{
    static float BUTTON_SPACE = 10;
    static MessageBox Active;

    public GameObject buttonPrefab;
    public TextMeshProUGUI text;
    public RectTransform textRect;
    public RectTransform borderLeft;
    public RectTransform borderRight;
    public RectTransform borderTop;
    public RectTransform borderBottom;

    RectTransform rect;
    List<MessageBoxButton> buttons = new List<MessageBoxButton>();

    void Start()
    {
        Active = this;
        rect = GetComponent<RectTransform>();
        gameObject.SetActive(false);
    }

    public static void Show(string message)
    {
        Active.gameObject.SetActive(true);
        Active.text.text = message;
        Active.textRect.sizeDelta = new Vector2(Active.textRect.sizeDelta.x, Active.text.preferredHeight);
        Active.rect.sizeDelta = new Vector2(Active.rect.sizeDelta.x, Active.textRect.sizeDelta.y + 90);
        Active.borderLeft.sizeDelta = Active.borderRight.sizeDelta = new Vector2(1, Active.rect.sizeDelta.y);
        Active.borderTop.sizeDelta = Active.borderBottom.sizeDelta = new Vector2(Active.rect.sizeDelta.x, 1);
    }

    public static void AddButton(string text, UnityAction callback = null)
    {
        GameObject go = Instantiate(Active.buttonPrefab, Active.transform);
        MessageBoxButton button = go.GetComponent<MessageBoxButton>();
        if (callback != null)
            button.button.onClick.AddListener(callback);
        button.text.text = text;
        button.rect.sizeDelta = new Vector2(Mathf.Max(button.text.preferredWidth + 30, 55), button.rect.sizeDelta.y);
        Active.buttons.Add(button);
        float totalWidth = 0;
        foreach (MessageBoxButton btn in Active.buttons)
        {
            totalWidth += btn.rect.sizeDelta.x + BUTTON_SPACE;
        }
        float x = (-totalWidth + BUTTON_SPACE) / 2, y = Active.textRect.anchoredPosition.y - Active.text.preferredHeight / 2;
        foreach (MessageBoxButton btn in Active.buttons)
        {
            btn.rect.anchoredPosition = new Vector2(x, y);
            x += btn.rect.sizeDelta.x + BUTTON_SPACE;
        }
    }

    public static void Hide()
    {
        foreach(MessageBoxButton button in Active.buttons)
        {
            Destroy(button.gameObject);
        }
        Active.buttons.Clear();
        Active.gameObject.SetActive(false);
    }
}
