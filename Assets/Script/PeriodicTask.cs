﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeriodicTask : MonoBehaviour
{
    static PeriodicTask Active;
    static bool Cancelled = false;

    public void Start()
    {
        Active = this;
    }

    void OnApplicationQuit()
    {
        if (Active == this)
            Active = null;
    }

    public static Coroutine StartPeriodicTask(float interval, Action act)
    {
        if (Active)
            return Active.StartCoroutine(Active.PeriodicTaskCoroutine(interval, act));
        else
            return null;
    }

    public static Coroutine DelayedAction(float delay, Action act)
    {
        if (Active)
            return Active.StartCoroutine(Active.DelayedTaskCoroutine(delay, act));
        else
            return null;
    }

    public static void StopTask(Coroutine task)
    {
        if (Active)
            Active.StopCoroutine(task);
    }

    public static void StopCurrentTask()
    {
        if (Active)
            Cancelled = true;
    }

    IEnumerator PeriodicTaskCoroutine(float interval, Action action)
    {
        while (!Cancelled)
        {
            yield return new WaitForSeconds(interval);
            action();
        }
        Cancelled = false;
    }

    IEnumerator DelayedTaskCoroutine(float delay, Action action)
    {
        yield return new WaitForSeconds(delay);
        action();
    }
}