﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CPanel : MonoBehaviour, IPointerDownHandler
{
    static Color INACTIVE_HEADER_COLOR = new Color(0.1f, 0.1f, 0.1f);

    bool isActive = false;
    public bool active
    {
        get
        {
            return isActive;
        }
        set
        {
            isActive = value;
            if (isActive)
            {
                headerImg.color = headerColor;
                canvasGroup.alpha = 1f;
                transform.parent = null;
                transform.parent = workspace.transform;
            }
            else
            {
                headerImg.color = INACTIVE_HEADER_COLOR;
                canvasGroup.alpha = 0.7f;
            }
        }
    }

    CanvasGroup canvasGroup;
    Color headerColor;

    [HideInInspector] public Workspace workspace;
    [HideInInspector] public RectTransform rect;
    public Image headerImg;

    void Awake()
    {
        rect = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        headerColor = headerImg.color;
        gameObject.SetActive(false);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        workspace.SetActivePanel(this);
    }
}
