﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPanel : MonoBehaviour
{
    const int DISPLAYED_ITEM = 6;
    const int TOTAL_ITEM = 8;
    const float ROW_HEIGHT = 105;

    static Vector2 SlideSpeed = 5 * Vector2.right;
    static Vector2 ScrollSpeed = 5 * Vector2.up;

    public RectTransform iconContainer;
    public Workspace workspace;

    bool visible = false;
    RectTransform rect;

    Vector2 bound;
    int scrollDex = 0;
    bool scrolling = false;

    void Start()
    {
        rect = GetComponent<RectTransform>();
        bound.x = iconContainer.anchoredPosition.y;
        bound.y = bound.x + 8 * 105f;
    }

	void FixedUpdate()
    {
        if (visible)
        {
            if (Input.mousePosition.x > 100)
            {
                visible = false;
            }
        }
        else
        {
            if (Input.mousePosition.x < 5 && Input.mousePosition.y > 763)
            {
                visible = true;
            }
        }

        if (visible)
        {
            if (rect.anchoredPosition.x < 49)
            {
                rect.anchoredPosition += SlideSpeed;
            }
        }
        else
        {
            if (rect.anchoredPosition.x > -49)
            {
                rect.anchoredPosition -= SlideSpeed;
            }
        }

        if (visible)// && !scrolling)
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0f)
            {
                ScrollUp();
            }
            else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
            {
                ScrollDown();
            }
        }

        if (scrolling)
        {
            float y = bound.x + ROW_HEIGHT * scrollDex;
            if (iconContainer.anchoredPosition.y != y)
            {
                if (iconContainer.anchoredPosition.y > y)
                {
                    iconContainer.anchoredPosition -= ScrollSpeed;
                    if (iconContainer.anchoredPosition.y <= y)
                    {
                        iconContainer.anchoredPosition = new Vector2(0, y);
                        scrolling = false;
                    }
                }
                else
                {
                    iconContainer.anchoredPosition += ScrollSpeed;
                    if (iconContainer.anchoredPosition.y >= y)
                    {
                        iconContainer.anchoredPosition = new Vector2(0, y);
                        scrolling = false;
                    }
                }
            }
        }
    }

    public void ScrollUp()
    {
        scrollDex--;
        if (scrollDex < 0)
            scrollDex = 0;
        scrolling = true;
    }

    public void ScrollDown()
    {
        scrollDex++;
        if (scrollDex > TOTAL_ITEM - DISPLAYED_ITEM)
            scrollDex = TOTAL_ITEM - DISPLAYED_ITEM;
        scrolling = true;
    }

    public void ActivatePanel(CPanel panel)
    {
        if (!panel.gameObject.activeSelf || workspace.ActivePanel != panel)
        {
            workspace.SetActivePanel(panel);
        }
        else
        {
            workspace.DeactivatePanel(panel);
        }
    }

    public void PlaySound(AudioSource snd)
    {
        snd.Play();
    }

    public void ExitProgram()
    {
        MessageBox.Show("Exit from the program?");
        MessageBox.AddButton("Yes", () =>
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        });
        MessageBox.AddButton("No");
    }
}
