﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

[CustomEditor(typeof(Table))]
[RequireComponent(typeof(Table))]
public class TableEditor : Editor
{
    GameObject temp;

    List<string> Contents;
    Texture deleteIcon;
    Texture editIcon;

    GUIStyle warning = new GUIStyle();
    GUIStyle deleteWarning = new GUIStyle();
    GUIStyle bold = new GUIStyle();
    GUIStyle doneBUtton = new GUIStyle();
    Scene scene;

    int editColumnIndex = -1;
    bool confirmingDelete = false;
    bool confirmingReset = false;
    int deleteTarget = -1;

    float def_labelWidth;
    float def_fieldWidth;
    Color def_guiColor;

    void OnEnable()
    {
        temp = new GameObject();
        scene = EditorSceneManager.GetActiveScene();
        deleteIcon = (Texture)AssetDatabase.LoadAssetAtPath("Assets/Texture/Table/Close.png", typeof(Texture));
        editIcon = (Texture)AssetDatabase.LoadAssetAtPath("Assets/Texture/Table/Setting.png", typeof(Texture));
        def_labelWidth = EditorGUIUtility.labelWidth;
        def_fieldWidth = EditorGUIUtility.fieldWidth;
        def_guiColor = GUI.color;

        warning.alignment = TextAnchor.MiddleRight;
        warning.fontSize = 10;
        warning.normal.textColor = Color.red;
        deleteWarning.alignment = TextAnchor.MiddleCenter;
        deleteWarning.fontSize = 11;
        deleteWarning.fontStyle = FontStyle.Normal;
        deleteWarning.normal.textColor = Color.red;
        bold.alignment = TextAnchor.MiddleLeft;
        bold.fontSize = 11;
        bold.fontStyle = FontStyle.Bold;
        doneBUtton.fontSize = 14;
        doneBUtton.fixedHeight = 30;
        doneBUtton.fixedWidth = 200;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Table table = (Table)target;
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        EditorGUILayout.LabelField("Table Editor", bold);
        if (GUILayout.Button("Refresh Table"))
        {
            table.RefreshProperties();
            table.RefreshColumnWidths();
        }
        if (GUILayout.Button("Reset Table"))
        {
            confirmingReset = true;
        }
        if (confirmingReset)
        {
            EditorGUILayout.LabelField("Confirm reset this table?", deleteWarning);
            EditorGUILayout.LabelField("(can't be undone)", deleteWarning);
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Yes"))
            {
                table.ClearTable();
                editColumnIndex = -1;
                confirmingReset = false;
                return;
            }
            if (GUILayout.Button("No"))
            {
                editColumnIndex = -1;
                confirmingReset = false;
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        EditorGUILayout.LabelField("Columns", bold);
        if (GUILayout.Button("Add New Column"))
        {
            editColumnIndex = -1;
            table.AddNewColumn();
            Undo.RegisterCompleteObjectUndo(table.gameObject, "add column to " + table.name);
            EditorUtility.SetDirty(table);
            EditorSceneManager.MarkSceneDirty(scene);
        }

        EditorGUI.BeginChangeCheck();
        Undo.RecordObject(table, "modify column name of " + table.name);
        EditorGUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = 1;
        EditorGUIUtility.fieldWidth = 10;
        for (int i = 0; i < table.Columns.Count; i++)
        {
            table.Columns[i].name = EditorGUILayout.TextField("", table.Columns[i].name);
        }
        EditorGUIUtility.labelWidth = def_labelWidth;
        EditorGUIUtility.fieldWidth = def_fieldWidth;
        EditorGUILayout.EndHorizontal();

        Undo.RecordObject(temp, "");

        EditorGUILayout.BeginHorizontal();
        for (int i = 0; i < table.Columns.Count; i++)
        {
            if (i == editColumnIndex)
                GUI.color = Color.gray;
            if (GUILayout.Button(editIcon))
            {
                if (editColumnIndex != i)
                    editColumnIndex = i;
                else
                    editColumnIndex = -1;
                GUI.FocusControl(null);
            }
            GUI.color = def_guiColor;
            if (GUILayout.Button(deleteIcon))
            {
                deleteTarget = i;
                confirmingDelete = true;
                GUI.FocusControl(null);
            }
        }
        EditorGUILayout.EndHorizontal();
        if (confirmingDelete)
        {
            EditorGUILayout.LabelField("Confirm delete column \"" + table.Columns[deleteTarget].name + "\"?", deleteWarning);
            EditorGUILayout.LabelField("(can't be undone)", deleteWarning);
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Yes"))
            {
                editColumnIndex = -1;
                table.DeleteColumn(deleteTarget);
                confirmingDelete = false;
                return;
            }
            if (GUILayout.Button("No"))
            {
                editColumnIndex = -1;
                confirmingDelete = false;
            }
            EditorGUILayout.EndHorizontal();
        }
        if (editColumnIndex > -1)
        {
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            EditorGUILayout.LabelField("Edit Column Properties", bold);
            Undo.RecordObject(table, "modify column property of " + table.name);

            // Property edit
            table.Columns[editColumnIndex].name = EditorGUILayout.TextField("Column Name", table.Columns[editColumnIndex].name);
            table.Columns[editColumnIndex].width = EditorGUILayout.Slider("Column Width", table.Columns[editColumnIndex].width, 0, Table.MAX_COLUMN_WIDTH);
            table.Columns[editColumnIndex].contentLeftMargin = EditorGUILayout.Slider("Content Left Margin", table.Columns[editColumnIndex].contentLeftMargin, 0, table.Columns[editColumnIndex].width);
            table.Columns[editColumnIndex].contentRightMargin = EditorGUILayout.Slider("Content Right Margin", table.Columns[editColumnIndex].contentRightMargin, 0, table.Columns[editColumnIndex].width - table.Columns[editColumnIndex].contentLeftMargin);
            table.Columns[editColumnIndex].headerAlignment = (TMPro.TextAlignmentOptions)EditorGUILayout.EnumPopup("Header Alignment", table.Columns[editColumnIndex].headerAlignment);
            table.Columns[editColumnIndex].alignment = (TMPro.TextAlignmentOptions)EditorGUILayout.EnumPopup("Content Alignment", table.Columns[editColumnIndex].alignment);
            table.Columns[editColumnIndex].contentType = (TMPro.TMP_InputField.ContentType)EditorGUILayout.EnumPopup("Content Type", table.Columns[editColumnIndex].contentType);
            table.Columns[editColumnIndex].helperType = (Table.HelperType)EditorGUILayout.EnumPopup("Input Helper", table.Columns[editColumnIndex].helperType);

            // Show warning
            if (table.Columns[editColumnIndex].helperType == Table.HelperType.Scroller && table.Columns[editColumnIndex].contentType != TMPro.TMP_InputField.ContentType.IntegerNumber && table.Columns[editColumnIndex].contentType != TMPro.TMP_InputField.ContentType.DecimalNumber)
            {
                EditorGUILayout.LabelField("*scroller only works for numeric content type!", warning);
            }
            table.Columns[editColumnIndex].maxLength = EditorGUILayout.IntField("Content Max Length", table.Columns[editColumnIndex].maxLength);
            table.Columns[editColumnIndex].readOnly = EditorGUILayout.Toggle("Read Only", table.Columns[editColumnIndex].readOnly);
            table.Columns[editColumnIndex].interactable = EditorGUILayout.Toggle("Interactable", table.Columns[editColumnIndex].interactable);

            Undo.RecordObject(temp, "");

            // Nudge control
            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginDisabledGroup(editColumnIndex == 0);
            if (GUILayout.Button("Move Left"))
            {
                table.SwapColumn(editColumnIndex, editColumnIndex - 1);
                editColumnIndex--;
                EditorSceneManager.MarkSceneDirty(scene);
                GUI.FocusControl(null);
            }
            EditorGUI.EndDisabledGroup();
            EditorGUI.BeginDisabledGroup(editColumnIndex == table.Columns.Count - 1);
            if (GUILayout.Button("Move Right"))
            {
                table.SwapColumn(editColumnIndex, editColumnIndex + 1);
                editColumnIndex++;
                EditorSceneManager.MarkSceneDirty(scene);
                GUI.FocusControl(null);
            }
            EditorGUI.EndDisabledGroup();
            EditorGUILayout.EndHorizontal();

            // Finish edit
            EditorGUILayout.Separator();
            GUI.color = Color.green;
            if (GUILayout.Button("Done"))
            {
                GUI.FocusControl(null);
                editColumnIndex = -1;
            }
            GUI.color = def_guiColor;
        }
        if (EditorGUI.EndChangeCheck())
        {
            table.RefreshColumnWidths();
            EditorUtility.SetDirty(table);
        }

        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        EditorGUILayout.LabelField("Rows", bold);

        if (table.Columns.Count > 0)
        {
            if (table.Columns[0].fields.Count > 0)
            {
                if (GUILayout.Button("Add Row"))
                {
                    table.AddNewRow();
                    Undo.RegisterCompleteObjectUndo(table.gameObject, "add row to " + table.name);
                    EditorUtility.SetDirty(table);
                    EditorSceneManager.MarkSceneDirty(scene);
                }
            }

            EditorGUIUtility.labelWidth = 1;
            EditorGUIUtility.fieldWidth = 10;
            EditorGUI.BeginChangeCheck();
            for (int i = 0; i < table.Columns[0].fields.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField((i + 1) + ".", bold);
                for (int j = 0; j < table.Columns.Count; j++)
                {
                    Undo.RecordObject(table.Columns[j].fields[i].inputField, "modify row property of " + table.name);
                    table.Columns[j].fields[i].text = EditorGUILayout.TextField("", table.Columns[j].fields[i].text);
                    Undo.RecordObject(temp, "");
                }
                if (GUILayout.Button(deleteIcon))
                {
                    table.DeleteRow(i);
                }
                EditorGUILayout.EndHorizontal();
            }
            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(table);
            }
            EditorGUIUtility.labelWidth = def_labelWidth;
            EditorGUIUtility.fieldWidth = def_fieldWidth;
        }

        if (GUILayout.Button("Add Row"))
        {
            table.AddNewRow();
            Undo.RegisterCompleteObjectUndo(table.gameObject, "add row to " + table.name);
            EditorUtility.SetDirty(table);
            EditorSceneManager.MarkSceneDirty(scene);
        }
    }
}
#endif