﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class VerticalLine : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    public Texture2D cursor;
    
    [HideInInspector] public RectTransform rect;
    [HideInInspector] public Table table;
    [HideInInspector] public int dataIndex;

    bool isDragging = false;
    float dragStartX;
    float dragStartWidth;

    public void OnPointerEnter(PointerEventData eventData)
    {
        Cursor.SetCursor(cursor, 256 * Vector2.one, CursorMode.Auto);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!isDragging)
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isDragging = true;
        dragStartX = Input.mousePosition.x;
        dragStartWidth = table.Columns[dataIndex].width;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        isDragging = false;
    }

    void Update()
    {
        if (isDragging)
        {
            table.Columns[dataIndex].width = Mathf.Max(dragStartWidth + (Input.mousePosition.x - dragStartX), 0);
            table.Columns[dataIndex].table.RefreshColumnWidths();
        }
    }
}
