﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TableField : MonoBehaviour
{
    public TMP_InputField inputField;
    public RectTransform rect;
    [HideInInspector] public RectTransform horizontalLine;

    public string text
    {
        get
        {
            return inputField.text;
        }
        set
        {
            if (inputField.text != value)
                inputField.text = value;
        }
    }
}
