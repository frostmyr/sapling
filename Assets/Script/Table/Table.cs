﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[ExecuteInEditMode]
public class Table : MonoBehaviour
{
    public const float MAX_COLUMN_WIDTH = 400;
    [Header("Config")]
    public Transform VerticalLineContainer;
    public Transform HorizontalLineContainer;
    public Transform ContentContainer;
    public Transform HeaderContainer;
    public GameObject VerticalLinePrefab;
    public GameObject HorizontalLinePrefab;
    public GameObject HeaderNamePrefab;
    public GameObject ColumnPrefab;
    public GameObject FieldPrefab;

    [Header("Layout")]
    public static float HEADER_HEIGHT = 30;
    public static float ROW_HEIGHT = 18;

    [SerializeField]
    [HideInInspector]
    public List<ColumnData> Columns;

    public enum HelperType
    {
        None, Dropdown, DateTimePicker, Scroller
    }

    [System.Serializable]
    public class ColumnData
    {
        [SerializeField]
        private string _name = "Column";
        public string name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = columnObj.gameObject.name = columnHeader.text = columnHeader.gameObject.name = value;
            }
        }

        [SerializeField]
        [Range(0, MAX_COLUMN_WIDTH)]
        private float _width = 100;
        public float width
        {
            get
            {
                return _width;
            }
            set
            {
                _width = value;
                columnObj.sizeDelta = columnHeaderRect.sizeDelta = new Vector2(value, HEADER_HEIGHT);
                columnLine.rect.anchoredPosition = new Vector2(columnObj.anchoredPosition.x + columnObj.sizeDelta.x, columnLine.rect.anchoredPosition.y);
            }
        }

        [SerializeField]
        [Range(0, MAX_COLUMN_WIDTH)]
        private float _contentLeftMargin = 5;
        public float contentLeftMargin
        {
            get
            {
                return _contentLeftMargin;
            }
            set
            {
                _contentLeftMargin = value;
                foreach (TableField field in fields)
                    field.rect.offsetMin = new Vector2(value, field.rect.offsetMin.y);
            }
        }

        [SerializeField]
        [Range(0, MAX_COLUMN_WIDTH)]
        private float _contentRightMargin = 5;
        public float contentRightMargin
        {
            get
            {
                return _contentRightMargin;
            }
            set
            {
                _contentRightMargin = value;
                foreach (TableField field in fields)
                    field.rect.offsetMax = new Vector2(-value, field.rect.offsetMax.y);
            }
        }

        [SerializeField]
        [Range(0, 32576)]
        private int _maxLength = 0;
        public int maxLength
        {
            get
            {
                return _maxLength;
            }
            set
            {
                _maxLength = value;
                foreach (TableField field in fields)
                    field.inputField.characterLimit = value;
            }
        }

        [SerializeField]
        private bool _readOnly = false;
        public bool readOnly
        {
            get
            {
                return _readOnly;
            }
            set
            {
                _readOnly = value;
                foreach (TableField field in fields)
                    field.inputField.readOnly = value;
            }
        }

        [SerializeField]
        private bool _interactable = false;
        public bool interactable
        {
            get
            {
                return _interactable;
            }
            set
            {
                _interactable = value;
                foreach (TableField field in fields)
                    field.inputField.interactable = value;
            }
        }

        [SerializeField]
        private HelperType _helperType = HelperType.None;
        public HelperType helperType
        {
            get
            {
                return _helperType;
            }
            set
            {
                _helperType = value;
            }
        }

        [SerializeField]
        private TextAlignmentOptions _headerAlignment = TextAlignmentOptions.Center;
        public TextAlignmentOptions headerAlignment
        {
            get
            {
                return _headerAlignment;
            }
            set
            {
                _headerAlignment = columnHeader.alignment = value;
            }
        }

        [SerializeField]
        private TextAlignmentOptions _alignment = TextAlignmentOptions.Left;
        public TextAlignmentOptions alignment
        {
            get
            {
                return _alignment;
            }
            set
            {
                _alignment = value;
                foreach (TableField field in fields)
                    field.inputField.textComponent.alignment = value;
            }
        }

        [SerializeField]
        private TMP_InputField.ContentType _contentType = TMP_InputField.ContentType.Standard;
        public TMP_InputField.ContentType contentType
        {
            get
            {
                return _contentType;
            }
            set
            {
                _contentType = value;
                foreach (TableField field in fields)
                    field.inputField.contentType = value;
            }
        }
        public List<TableField> fields = new List<TableField>();
        [HideInInspector] public RectTransform columnObj;
        [HideInInspector] public RectTransform columnHeaderRect;
        [HideInInspector] public TextMeshProUGUI columnHeader;
        [HideInInspector] public VerticalLine columnLine;
        [HideInInspector] public Table table;
    }

    public ColumnData AddNewColumn()
    {
        ColumnData column = new ColumnData();

        // Create header
        GameObject columnHeader = Instantiate(HeaderNamePrefab, HeaderContainer);
        columnHeader.name = "Column " + (Columns.Count + 1);
#if UNITY_EDITOR
        UnityEditor.Undo.RegisterCreatedObjectUndo(columnHeader, "add column to " + name);
#endif
        column.columnHeader = columnHeader.GetComponent<TextMeshProUGUI>();
        column.columnHeaderRect = columnHeader.GetComponent<RectTransform>();
        if (Columns.Count > 0)
            column.columnHeaderRect.anchoredPosition = new Vector2(Columns[Columns.Count - 1].columnHeaderRect.anchoredPosition.x + Columns[Columns.Count - 1].columnHeaderRect.sizeDelta.x, column.columnHeaderRect.anchoredPosition.y);

        // Create column container
        GameObject columnObj = Instantiate(ColumnPrefab, ContentContainer);
#if UNITY_EDITOR
        UnityEditor.Undo.RegisterCreatedObjectUndo(columnObj, "add column to " + name);
#endif
        columnObj.name = columnHeader.name;
        column.columnObj = columnObj.GetComponent<RectTransform>();
        if (Columns.Count > 0)
            column.columnObj.anchoredPosition = new Vector2(Columns[Columns.Count - 1].columnObj.anchoredPosition.x + Columns[Columns.Count - 1].columnObj.sizeDelta.x, column.columnObj.anchoredPosition.y);

        // Create line
        GameObject columnLine = Instantiate(VerticalLinePrefab, VerticalLineContainer);
#if UNITY_EDITOR
        UnityEditor.Undo.RegisterCreatedObjectUndo(columnLine, "add column to " + name);
#endif
        column.columnLine = columnLine.GetComponent<VerticalLine>();
        column.columnLine.rect = columnLine.GetComponent<RectTransform>();
        column.columnLine.table = this;
        column.columnLine.dataIndex = Columns.Count;
        column.columnLine.rect.anchoredPosition = new Vector2(column.columnObj.anchoredPosition.x + column.columnObj.sizeDelta.x, column.columnLine.rect.anchoredPosition.y);

        // init column props
        column.name = columnObj.name;
        column.table = this;
        Columns.Add(column);

        // Add fields
        for (int i = 0; i < Columns[0].fields.Count; i++)
        {
            GameObject fieldObj = Instantiate(FieldPrefab, column.columnObj);
#if UNITY_EDITOR
            UnityEditor.Undo.RegisterCreatedObjectUndo(fieldObj, "add column to " + name);
#endif
            TableField field = fieldObj.GetComponent<TableField>();
            field.rect.anchoredPosition = new Vector2(0, -ROW_HEIGHT * column.fields.Count);
            field.inputField.textComponent.alignment = column.alignment;
            field.inputField.characterLimit = column.maxLength;
            field.inputField.contentType = column.contentType;
            field.inputField.readOnly = column.readOnly;
            field.inputField.interactable = column.interactable;
            column.fields.Add(field);
            fieldObj.name = "Field " + column.fields.Count;
        }

        return column;
    }

    public void DeleteColumn(int i)
    {
        ColumnData column = Columns[i];
        DestroyImmediate(column.columnLine.gameObject);
        DestroyImmediate(column.columnObj.gameObject);
        DestroyImmediate(column.columnHeader.gameObject);
        Columns.RemoveAt(i);
        RefreshColumnWidths();
    }

    public void ClearTable()
    {
        for (int i = Columns.Count - 1; i >= 0; i--)
        {
            ColumnData column = Columns[i];
            DestroyImmediate(column.columnLine.gameObject);
            DestroyImmediate(column.columnObj.gameObject);
            DestroyImmediate(column.columnHeader.gameObject);
            Columns.RemoveAt(i);
        }
    }

    public void SwapColumn(int from, int to)
    {
#if UNITY_EDITOR
        UnityEditor.Undo.RecordObject(this, "swap column of " + name);
#endif
        ColumnData s = Columns[from];
        Columns[from] = Columns[to];
        Columns[to] = s;
        RefreshColumnWidths();
    }

    public void AddNewRow()
    {
        for (int i = 0; i < Columns.Count; i++)
        {
            ColumnData c = Columns[i];
            GameObject fieldObj = Instantiate(FieldPrefab, c.columnObj);
#if UNITY_EDITOR
            UnityEditor.Undo.RegisterCreatedObjectUndo(fieldObj, "add row to " + name);
#endif
            TableField field = fieldObj.GetComponent<TableField>();
            field.rect.anchoredPosition = new Vector2(0, -ROW_HEIGHT * c.fields.Count);
            field.inputField.textComponent.alignment = c.alignment;
            field.inputField.characterLimit = c.maxLength;
            field.inputField.contentType = c.contentType;
            field.inputField.readOnly = c.readOnly;
            field.inputField.interactable = c.interactable;
            field.rect.offsetMin = new Vector2(c.contentLeftMargin, field.rect.offsetMin.y);
            field.rect.offsetMax = new Vector2(-c.contentRightMargin, field.rect.offsetMax.y);
            c.fields.Add(field);
            fieldObj.name = "Field " + c.fields.Count;
        }
    }

    public void DeleteRow(int index)
    {
        for (int i = 0; i < Columns.Count; i++)
        {
            ColumnData c = Columns[i];
            GameObject go = c.fields[index].gameObject;
            c.fields.RemoveAt(index);
            DestroyImmediate(go);
            for (int j = index; j < c.fields.Count; j++)
            {
                c.fields[j].rect.anchoredPosition = new Vector2(0, -ROW_HEIGHT * j);
                c.fields[j].rect.offsetMin = new Vector2(c.contentLeftMargin, c.fields[j].rect.offsetMin.y);
                c.fields[j].rect.offsetMax = new Vector2(-c.contentRightMargin, c.fields[j].rect.offsetMax.y);
            }
        }
    }

    public string GetContent(int row, int col)
    {
        return Columns[col].fields[row].inputField.text;
    }

    public void SetContent(int row, int col, string val)
    {
        Columns[col].fields[row].inputField.text = val;
    }

    public void RefreshColumnWidths()
    {
        if (Columns.Count > 0)
        {
            Columns[0].columnObj.anchoredPosition = Vector2.zero;
            Columns[0].columnHeaderRect.anchoredPosition = Vector2.zero;
            for (int i = 1; i < Columns.Count; i++)
            {
                ColumnData c = Columns[i];
                ColumnData prev = Columns[i - 1];
                c.columnObj.anchoredPosition = new Vector2(prev.columnObj.anchoredPosition.x + prev.columnObj.sizeDelta.x, c.columnObj.anchoredPosition.y);
                c.columnHeaderRect.anchoredPosition = new Vector2(prev.columnHeaderRect.anchoredPosition.x + prev.columnHeaderRect.sizeDelta.x, c.columnHeaderRect.anchoredPosition.y);
                c.columnLine.rect.anchoredPosition = new Vector2(c.columnObj.anchoredPosition.x + c.columnObj.sizeDelta.x, c.columnLine.rect.anchoredPosition.y);
            }
        }
    }

    public void RefreshProperties()
    {
        for (int i = 0; i < Columns.Count; i++)
        {
            ColumnData c = Columns[i];
            c.name = c.name;
            c.headerAlignment = c.headerAlignment;
            c.alignment = c.alignment;
            c.readOnly = c.readOnly;
            c.contentType = c.contentType;
            c.contentLeftMargin = c.contentLeftMargin;
            c.contentRightMargin = c.contentRightMargin;
            c.maxLength = c.maxLength;
            c.width = c.width;
            if (i > 0)
            {
                ColumnData prev = Columns[i - 1];
                c.columnObj.anchoredPosition = new Vector2(prev.columnObj.anchoredPosition.x + prev.columnObj.sizeDelta.x, c.columnObj.anchoredPosition.y);
            }
        }
    }

    void OnValidate()
    {
        RefreshProperties();
    }
}
