﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ShowTooltips : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [TextArea]
    public string TooltipsText;
    public Tooltips.TooltipsPosition TooltipsPosition = Tooltips.TooltipsPosition.Right;

    RectTransform rect;

    public void OnPointerEnter(PointerEventData eventData)
    {
        Tooltips.Show(TooltipsText, rect, TooltipsPosition);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Tooltips.Hide();
    }

    void OnDisable()
    {
        Tooltips.Hide();
    }

    void Start()
    {
        rect = GetComponent<RectTransform>();
    }
}
