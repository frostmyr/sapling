﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace FullScreenPlayModes
{
    [InitializeOnLoad]
    public class FullScreenPlayMode : Editor
    {
        private static Rect previousPos;
        private static Vector2 previousSize;
        //The size of the toolbar above the game view, excluding the OS border.
        private static int toolbarHeight = 22;

        static FullScreenPlayMode()
        {
            EditorApplication.playModeStateChanged -= PlayModeStateChanged;
            EditorApplication.playModeStateChanged += PlayModeStateChanged;
        }

        static void PlayModeStateChanged(PlayModeStateChange _playModeStateChange)
        {
            if (PlayerPrefs.GetInt("PlayMode_FullScreen", 0) == 1)
            {
                // Get game editor window
                EditorApplication.ExecuteMenuItem("Window/Game");
                System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
                System.Reflection.MethodInfo GetMainGameView = T.GetMethod("GetMainGameView", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
                System.Object Res = GetMainGameView.Invoke(null, null);
                EditorWindow gameView = (EditorWindow)Res;

                switch (_playModeStateChange)
                {
                    case PlayModeStateChange.EnteredPlayMode:

                        Rect newPos = new Rect(0, 0 - toolbarHeight+5, Screen.currentResolution.width, Screen.currentResolution.height + toolbarHeight);

                        gameView.position = newPos;
                        gameView.minSize = new Vector2(Screen.currentResolution.width, Screen.currentResolution.height + toolbarHeight);
                        gameView.maxSize = gameView.minSize;
                        gameView.position = newPos;

                        break;

                    case PlayModeStateChange.EnteredEditMode:

                        gameView.Close();

                        break;
                }
            }
        }

        [MenuItem("Play Mode/Default/Full Screen", false, 0)]
        public static void PlayModeFullScreen()
        {
            PlayerPrefs.SetInt("PlayMode_FullScreen", 1);
        }

        [MenuItem("Play Mode/Default/Full Screen", true, 0)]
        public static bool PlayModeFullScreenValidate()
        {
            return PlayerPrefs.GetInt("PlayMode_FullScreen", 0) == 0;
        }

        [MenuItem("Play Mode/Default/Window", false, 0)]
        public static void PlayModeWindow()
        {
            PlayerPrefs.SetInt("PlayMode_FullScreen", 0);
        }

        [MenuItem("Play Mode/Default/Window", true, 0)]
        public static bool PlayModeWindowValidate()
        {
            return PlayerPrefs.GetInt("PlayMode_FullScreen", 0) == 1;
        }

        [MenuItem("Play Mode/Enter Fullscreen %g")]
        public static void EnterFullScreen()
        {
            EditorApplication.ExecuteMenuItem("Window/Game");
            System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
            System.Reflection.MethodInfo GetMainGameView = T.GetMethod("GetMainGameView", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
            System.Object Res = GetMainGameView.Invoke(null, null);
            EditorWindow gameView = (EditorWindow)Res;

            Rect newPos = new Rect(0, 0 - toolbarHeight + 5, Screen.currentResolution.width, Screen.currentResolution.height + toolbarHeight);

            previousPos = gameView.position;
            previousSize = gameView.minSize;

            gameView.position = newPos;
            gameView.minSize = new Vector2(Screen.currentResolution.width, Screen.currentResolution.height + toolbarHeight);
            gameView.maxSize = gameView.minSize;
            gameView.position = newPos;
        }

        [MenuItem("Play Mode/Exit Fullscreen %h")]
        public static void ExitFullScreen()
        {
            EditorApplication.ExecuteMenuItem("Window/Game");
            System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
            System.Reflection.MethodInfo GetMainGameView = T.GetMethod("GetMainGameView", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
            System.Object Res = GetMainGameView.Invoke(null, null);
            EditorWindow gameView = (EditorWindow)Res;
            gameView.Close();

            EditorApplication.ExecuteMenuItem("Window/Game");
            T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
            GetMainGameView = T.GetMethod("GetMainGameView", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
            Res = GetMainGameView.Invoke(null, null);
            gameView = (EditorWindow)Res;
            gameView.position = previousPos;
            gameView.minSize = previousSize;
        }
    }
}
#endif