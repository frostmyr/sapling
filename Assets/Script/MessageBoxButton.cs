﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MessageBoxButton : MonoBehaviour
{
    public RectTransform rect;
    public TextMeshProUGUI text;
    public Button button;

    void Start()
    {
        button.onClick.AddListener(OnPress);
    }

    void OnPress()
    {
        MessageBox.Hide();
    }
}
