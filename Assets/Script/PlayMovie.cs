﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayMovie : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        MovieTexture tex = (MovieTexture)GetComponent<RawImage>().texture;
        tex.loop = true;
        tex.Play();
        //AudioSource src = gameObject.AddComponent<AudioSource>();
        //src.clip = tex.audioClip;
        //src.pitch = 0.5f;
        //src.Play();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
