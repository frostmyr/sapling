﻿Shader "Unlit/Blur"
{
	Properties
	{
		_Radius("Radius", Range(0, 10)) = 1
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Texture", 2D) = "black" {}
		_AlphaMask("Alpha Mask", 2D) = "white" {}
	}

	Category
	{
		Tags { "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }

		SubShader
		{
			GrabPass
			{
				Tags{ "LightMode" = "Always" }
			}

			Pass
			{
				Tags { "LightMode" = "Always" }

				CGPROGRAM

				#pragma vertex vert
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#include "UnityCG.cginc"

				struct appdata_t
				{
					float4 vertex : POSITION;
					float2 texcoord: TEXCOORD0;
				};

				struct v2f
				{
					float4 vertex : POSITION;
					float4 uvgrab : TEXCOORD0;
				};

				v2f vert(appdata_t v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
#if UNITY_UV_STARTS_AT_TOP
					float scale = -1.0;
#else
					float scale = 1.0;
#endif
					o.uvgrab.xy = (float2(o.vertex.x, o.vertex.y*scale) + o.vertex.w) * 0.5;
					o.uvgrab.zw = o.vertex.zw;
					return o;
				}

				sampler2D _GrabTexture;
				sampler2D _MainTex;
				sampler2D _AlphaMask;
				fixed4 _Color;
				float4 _GrabTexture_TexelSize;
				float _Radius;

				half4 frag(v2f i) : COLOR
				{
					half4 sum = half4(0,0,0,0);

#define GRABXYPIXEL(tex, kernelx, kernely) tex2Dproj( tex, UNITY_PROJ_COORD(float4(i.uvgrab.x + _GrabTexture_TexelSize.x * kernelx, i.uvgrab.y + _GrabTexture_TexelSize.y * kernely, i.uvgrab.z, i.uvgrab.w)))

					sum += GRABXYPIXEL(_GrabTexture, 0.0, 0.0);
					int measurments = 1;

					for (float range = 0.1f; range <= _Radius; range += 0.1f)
					{
						sum += GRABXYPIXEL(_GrabTexture, range, range);
						sum += GRABXYPIXEL(_GrabTexture, range, -range);
						sum += GRABXYPIXEL(_GrabTexture, -range, range);
						sum += GRABXYPIXEL(_GrabTexture, -range, -range);
						measurments += 4;
					}

					float alpha = GRABXYPIXEL(_AlphaMask, 0, 0);
					return (sum / measurments * (1 - _Color.a) * alpha + GRABXYPIXEL(_GrabTexture, 0, 0) * (1 - alpha)) + (_Color * _Color.a * alpha);
				}

				ENDCG
			}

		GrabPass
		{
			Tags{ "LightMode" = "Always" }
		}

		Pass
		{
			Tags{ "LightMode" = "Always" }

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"

			struct appdata_t
			{
				float4 vertex : POSITION;
				float2 texcoord: TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : POSITION;
				float4 uvgrab : TEXCOORD0;
			};

			v2f vert(appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
#if UNITY_UV_STARTS_AT_TOP
				float scale = -1.0;
#else
				float scale = 1.0;
#endif
				o.uvgrab.xy = (float2(o.vertex.x, o.vertex.y*scale) + o.vertex.w) * 0.5;
				o.uvgrab.zw = o.vertex.zw;
				return o;
			}

			sampler2D _GrabTexture;
			sampler2D _MainTex;
			sampler2D _AlphaMask;
			fixed4 _Color;
			float4 _GrabTexture_TexelSize;
			float _Radius;

			half4 frag(v2f i) : COLOR
			{

				half4 sum = half4(0,0,0,0);
				float radius = 1.41421356237 * _Radius;

#define GRABXYPIXEL(tex, kernelx, kernely) tex2Dproj( tex, UNITY_PROJ_COORD(float4(i.uvgrab.x + _GrabTexture_TexelSize.x * kernelx, i.uvgrab.y + _GrabTexture_TexelSize.y * kernely, i.uvgrab.z, i.uvgrab.w)))

				sum += GRABXYPIXEL(_GrabTexture, 0.0, 0.0);
				int measurments = 1;

				for (float range = 1.41421356237f; range <= radius * 1.41; range += 1.41421356237f)
				{
					sum += GRABXYPIXEL(_GrabTexture, range, 0);
					sum += GRABXYPIXEL(_GrabTexture, -range, 0);
					sum += GRABXYPIXEL(_GrabTexture, 0, range);
					sum += GRABXYPIXEL(_GrabTexture, 0, -range);
					measurments += 4;
				}

				float alpha = GRABXYPIXEL(_AlphaMask, 0, 0);
				return (sum / measurments * (1 - _Color.a) * alpha + GRABXYPIXEL(_GrabTexture, 0, 0) * (1-alpha)) + (_Color * _Color.a * alpha);
			}

			ENDCG

			}
		}
	}
}